version: "3.3"

services:
  grafana:
    image: grafana/grafana
    restart: on-failure:5
    container_name: grafana
    hostname: grafana
    profiles: &profiles
      - all
      - lvl3
    networks:
      - web
    depends_on:
      - traefik
      - authelia
    user: "${PUID}:${PGID}"
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
      - GF_SERVER_DOMAIN=grafana.${PUBLIC_DNS}
      - GF_DEFAULT_INSTANCE_NAME=Vizbab monitoring
      - GF_SECURITY_ADMIN_USER=Nindouja
      - GF_FEATURE_TOGGLES_ENABLE=newNavigation
      - GF_AUTH_ANONYMOUS_ENABLED=true
      - GF_AUTH_ANONYMOUS_ORG_ROLE=Viewer
      - GF_AUTH_ANONYMOUS_ORG_NAME=Vizbab
    ports:
      - 3002:3000
    volumes:
      - ${DATA_FOLDER}/grafana:/var/lib/grafana 
      - ${CONF_FOLDER}/grafana/datasources:/etc/grafana/provisioning/datasources/
      - ${CONF_FOLDER}/grafana/dashboards:/etc/grafana/provisioning/dashboards/
    
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=true
      - traefik.docker.network=web
      - traefik.http.routers.grafana.tls=true
      - traefik.http.routers.grafana.service=grafana
      - traefik.http.routers.grafana.entrypoints=https
      - traefik.http.routers.grafana.tls.certResolver=resolver
      - traefik.http.routers.grafana.rule=Host(`grafana.${PUBLIC_DNS}`)
      - traefik.http.routers.grafana.middlewares=authelia@docker
      - traefik.http.services.grafana.loadbalancer.server.port=3000
      - homepage.group=Monitoring
      - homepage.weight=1000
      - homepage.name=grafana
      - homepage.description=Monitoring dashboads
      - homepage.icon=grafana
      - homepage.href=http://grafana.${PUBLIC_DNS}/
  telegraf:
    image: telegraf
    restart: on-failure:5
    container_name: telegraf
    hostname: telegraf
    profiles: *profiles
    networks:
      - web
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
      - HOST_ETC=/hostfs/etc
      - HOST_PROC=/hostfs/proc
      - HOST_SYS=/hostfs/sys
      - HOST_VAR=/hostfs/var
      - HOST_RUN=/hostfs/run
      - HOST_MOUNT_PREFIX=/hostfs
    volumes:
      - /:/hostfs:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ${CONF_FOLDER}/telegraf/telegraf.conf:/etc/telegraf/telegraf.conf:ro
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=false
      - homepage.group=Monitoring
      - homepage.weight=1000
      - homepage.name=telegraf
      - homepage.description=Monitoring agent
      - homepage.icon=telegraf


  speedtest:
    image: lscr.io/linuxserver/speedtest-tracker
    restart: on-failure:5
    container_name: speedtest
    hostname: speedtest
    profiles: *profiles
    networks:
      - web
    depends_on:
      - traefik
      - authelia
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
      - DB_CONNECTION=mysql
      - DB_HOST=mariadb
      - DB_PORT=3306
      - DB_DATABASE=speedtest
      - DB_USERNAME=speedtest
      - DB_PASSWORD=${SPEEDTEST_MYSQL_PASSWD}
      - APP_KEY=${SPEEDTEST_ENCRYPT_KEY}
      - APP_NAME="Vizbab Speedtest Tracker"
      - PUBLIC_DASHBOARD=true
      - SPEEDTEST_SCHEDULE=0 * * * *
      - SPEEDTEST_SERVERS=63829,33869,24130,49027,27961,28308,62035,62667 # docker exec speedtest  php /app/www/artisan app:ookla-list-servers
      - PRUNE_RESULTS_OLDER_THAN=370
    ports:
      - 8765:80
    volumes:
      - ${CONF_FOLDER}/speedtest:/config
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=true
      - traefik.docker.network=web
      - traefik.http.routers.speedtest.tls=true
      - traefik.http.routers.speedtest.service=speedtest
      - traefik.http.routers.speedtest.entrypoints=https
      - traefik.http.routers.speedtest.tls.certResolver=resolver
      - traefik.http.routers.speedtest.rule=Host(`speed.${PUBLIC_DNS}`)
      - traefik.http.routers.speedtest.middlewares=authelia@docker
      - traefik.http.services.speedtest.loadbalancer.server.port=80
      - homepage.group=Monitoring
      - homepage.weight=1000
      - homepage.name=Speedtest
      - homepage.description=Get info about internet speed over time
      - homepage.icon=speedtest
      - homepage.href=http://speed.${PUBLIC_DNS}/
      - homepage.icon=speedtest-tracker
  picard:
    image: mikenye/picard
    restart: on-failure:5
    container_name: picard
    hostname: picard
    profiles: *profiles
    networks:
      - web
    depends_on:
      - traefik
      - authelia
      - radarr
      - sonarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
    ports:
      - 5800:5800
    volumes:
      - ${CONF_FOLDER}/picard:/config
      - ${MEDIA_FOLDER}/Music:/storage/public
      - ${PRIVATE_FOLDER}/media/Music:/storage/private
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=true
      - traefik.docker.network=web
      - traefik.http.routers.picard.tls=true
      - traefik.http.routers.picard.service=picard
      - traefik.http.routers.picard.entrypoints=https
      - traefik.http.routers.picard.tls.certResolver=resolver
      - traefik.http.routers.picard.rule=Host(`picard.${PUBLIC_DNS}`)
      - traefik.http.routers.picard.middlewares=authelia@docker
      - traefik.http.services.picard.loadbalancer.server.port=5800
      - homepage.group=Utilities
      - homepage.weight=1000
      - homepage.name=Musicbrainz picard
      - homepage.description=Help taging music in bulk
      - homepage.icon=musicbrainz
      - homepage.href=http://picard.${PUBLIC_DNS}/
  filebot:
    image: jlesage/filebot
    restart: on-failure:5
    container_name: filebot
    hostname: filebot
    profiles: *profiles
    networks:
      - web
    depends_on:
      - traefik
      - authelia
      - radarr
      - sonarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
    ports:
      - 5801:5800
    volumes:
      - ${CONF_FOLDER}/filebot:/config
      - ${MEDIA_FOLDER}:/storage
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=true
      - traefik.docker.network=web
      - traefik.http.routers.filebot.tls=true
      - traefik.http.routers.filebot.service=filebot
      - traefik.http.routers.filebot.entrypoints=https
      - traefik.http.routers.filebot.tls.certResolver=resolver
      - traefik.http.routers.filebot.rule=Host(`filebot.${PUBLIC_DNS}`)
      - traefik.http.routers.filebot.middlewares=authelia@docker
      - traefik.http.services.filebot.loadbalancer.server.port=5800
      - homepage.group=Utilities
      - homepage.weight=1000
      - homepage.name=Filebot
      - homepage.description=Help renaming medias in bulk
      - homepage.icon=filebot
      - homepage.href=http://filebot.${PUBLIC_DNS}/
  hrconvert2:
    image: dwaaan/hrconvert2-docker
    restart: on-failure:5
    container_name: hrconvert2
    hostname: hrconvert2
    profiles: *profiles
    networks:
      - web
    depends_on:
      - traefik
      - authelia
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
    ports:
      - 8585:80
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=true
      - traefik.docker.network=web
      - traefik.http.routers.hrconvert2.tls=true
      - traefik.http.routers.hrconvert2.service=hrconvert2
      - traefik.http.routers.hrconvert2.entrypoints=https
      - traefik.http.routers.hrconvert2.tls.certResolver=resolver
      - traefik.http.routers.hrconvert2.rule=Host(`convert.${PUBLIC_DNS}`)
      - traefik.http.routers.hrconvert2.middlewares=authelia@docker
      - traefik.http.services.hrconvert2.loadbalancer.server.port=80
      - homepage.group=Utilities
      - homepage.weight=1000
      - homepage.name=HRConvert
      - homepage.description=Convert and edit common file types
      - homepage.icon=mdi-file-move#000000
      - homepage.href=http://convert.${PUBLIC_DNS}/


  ollama:
    image: ollama/ollama
    restart: on-failure:5
    container_name: ollama
    hostname: ollama
    profiles: *profiles
    networks:
      - web
    depends_on:
      - traefik
      - authelia
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
    ports:
      - 11434:11434
    volumes:
      - ${DATA_FOLDER}/ollama:/root/.ollama
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=false
      - homepage.group=Others
      - homepage.weight=1000
      - homepage.name=Ollama
      - homepage.description=Self hosted LLM
      - homepage.icon=mdi-lama
      - homepage.href=http://ollama.${PUBLIC_DNS}/
  openui:
    image: ghcr.io/open-webui/open-webui:main
    restart: on-failure:5
    container_name: openui
    hostname: openui
    profiles: *profiles
    networks:
      - web
    depends_on:
      - traefik
      - authelia
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
      - OLLAMA_BASE_URL=http://ollama:11434
    ports:
      - 8832:8080
    volumes:
      - ${DATA_FOLDER}/openui:/app/backend/data
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=true
      - traefik.docker.network=web
      - traefik.http.routers.openui.tls=true
      - traefik.http.routers.openui.service=openui
      - traefik.http.routers.openui.entrypoints=https
      - traefik.http.routers.openui.tls.certResolver=resolver
      - traefik.http.routers.openui.rule=Host(`openui.${PUBLIC_DNS}`)
      - traefik.http.routers.openui.middlewares=authelia@docker
      - traefik.http.services.openui.loadbalancer.server.port=8080
      - homepage.group=Private cloud
      - homepage.weight=1000
      - homepage.name=Open web ui
      - homepage.description=UI for ollama
      - homepage.icon=mdi-robot
      - homepage.href=http://openui.${PUBLIC_DNS}/

  shlinkui:
    image: shlinkio/shlink-web-client
    restart: on-failure:5
    container_name: shlinkui
    hostname: shlinkui
    profiles: *profiles
    networks:
      - web
    depends_on:
      - traefik
      - authelia
    environment:
      - SHLINK_SERVER_URL=http://s.${PUBLIC_DNS}
      - SHLINK_SERVER_API_KEY=${SHLINK_API_KEY}
    ports:
      - 8499:8080
    volumes:
      - ${CONF_FOLDER}/shlinkui/:/usr/share/nginx/html/conf.d/
    labels:
      - com.centurylinklabs.watchtower.enable=true
      - traefik.enable=true
      - traefik.docker.network=web
      - traefik.http.routers.shlinkui.tls=true
      - traefik.http.routers.shlinkui.service=shlinkui
      - traefik.http.routers.shlinkui.entrypoints=https
      - traefik.http.routers.shlinkui.tls.certResolver=resolver
      - traefik.http.routers.shlinkui.rule=Host(`shlink.${PUBLIC_DNS}`)
      - traefik.http.routers.shlinkui.middlewares=authelia@docker
      - traefik.http.services.shlinkui.loadbalancer.server.port=8080
      - homepage.group=Utilities
      - homepage.weight=1000
      - homepage.name=Shlink UI
      - homepage.description=Web interface for shlink to shorten URLs
      - homepage.icon=shlink
      - homepage.href=http://shlink.${PUBLIC_DNS}/
