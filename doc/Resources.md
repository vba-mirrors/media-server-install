
# External accounts

## Usenet 

* [NZBGeek](https://nzbgeek.info/)
* [Tweaknews](https://www.tweaknews.eu/en/member)
* [drunkenslug](https://drunkenslug.com/)

## Torrent

[redacted](https://redacted.ch/)

    
## Subtitles

* [opensubtitles](https://www.opensubtitles.org)


## Others

* [maxmind](https://www.maxmind.com/) 
* [pushover](https://client.pushover.net/)
* 



# Help

# Helpfull soft

* https://github.com/bcicen/ctop
* https://forums.unraid.net/topic/55671-plugin-ca-auto-turbo-write-mode/
* 
## GitLab
* [install guide](https://docs.gitlab.com/ee/install/README.html)
* [debug mails](https://docs.gitlab.com/ee/administration/troubleshooting/debug.html)

## Telegraf
* [Install telegraf on windows](https://portal.influxdata.com/downloads/)
* [Run telegraf on start](https://www.influxdata.com/blog/using-telegraf-on-windows/
https://github.com/influxdata/telegraf/blob/master/docs/WINDOWS_SERVICE.md)

# Infra
## Debug A start job is running for Raise network interfaces
* https://forum.proxmox.com/threads/a-start-job-is-running-for-raise-network-interfaces.32936/


# Discord
* [server template](https://discord.new/k8nD6JWf3r4a)

# Homepage
* [apps icons](https://github.com/walkxcode/dashboard-icons)
* [material icons](https://pictogrammers.com/library/mdi)