# Vizbab

The main goal is to **have access to a private minimalistc cloud over the internet**. By minimalistic I mean that my main focus is being able to:
* read my media
* see and edit my code
* organize my finances


## Doc

https://grafana.com/grafana/dashboards/928-telegraf-system-dashboard/

# Docker profiles/levels

# Level 0 - `lvl0`

Needs to run if anything else run

# Level 1 - `lvl1`

Services required for the main goal of this project described above. So only te containers required to have those features are in the first level.

# Level 2 - `lvl2`

Extra cloud services that are not as useful.

# Level 3- `lvl3`

Stuff that requires less regular access; like manual youtube downloads and such.

# Level 4 - `lvl4`

Stuff in WIP
