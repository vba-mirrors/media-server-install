# photoprism
CREATE DATABASE photoprism
CHARACTER SET = 'utf8mb4'
COLLATE = 'utf8mb4_unicode_ci';

CREATE USER 'photoprism'@'%' IDENTIFIED BY '${PHOTOPRISM_MYSQL_PASSWD}';
GRANT ALL PRIVILEGES ON photoprism.* to 'photoprism'@'%';
FLUSH PRIVILEGES;

# shlink
CREATE DATABASE shlink
CHARACTER SET = 'utf8mb4'
COLLATE = 'utf8mb4_unicode_ci';

CREATE USER 'shlink'@'%' IDENTIFIED BY '${SHLINK_MYSQL_PASSWD}';
GRANT ALL PRIVILEGES ON shlink.* to 'shlink'@'%';
FLUSH PRIVILEGES;


# akaunting
CREATE DATABASE akaunting
CHARACTER SET = 'utf8mb4'
COLLATE = 'utf8mb4_unicode_ci';

CREATE USER 'akaunting'@'%' IDENTIFIED BY '${AKAUNTING_MYSQL_PASSWD}';
GRANT ALL PRIVILEGES ON akaunting.* to 'akaunting'@'%';
FLUSH PRIVILEGES;


# foofy
CREATE DATABASE foofy
CHARACTER SET = 'utf8mb4'
COLLATE = 'utf8mb4_unicode_ci';

CREATE USER 'foofy'@'%' IDENTIFIED BY '${FOOFY_MYSQL_PASSWD}';
GRANT ALL PRIVILEGES ON foofy.* to 'foofy'@'%';
FLUSH PRIVILEGES;


# les_ptites_resa
CREATE DATABASE les_ptites_resa
CHARACTER SET = 'utf8mb4'
COLLATE = 'utf8mb4_unicode_ci';

CREATE USER 'les_ptites_resa'@'%' IDENTIFIED BY '${LPR_MYSQL_PASSWD}';
GRANT ALL PRIVILEGES ON les_ptites_resa.* to 'les_ptites_resa'@'%';
FLUSH PRIVILEGES;


# vizbab
CREATE DATABASE vizbab
CHARACTER SET = 'utf8mb4'
COLLATE = 'utf8mb4_unicode_ci';

CREATE USER 'vizbab'@'%' IDENTIFIED BY '${VIZBAB_MYSQL_PASSWD}';
GRANT ALL PRIVILEGES ON vizbab.* to 'vizbab'@'%';
FLUSH PRIVILEGES;