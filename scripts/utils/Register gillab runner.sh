#!/bin/bash

gitlab-runner register \
    --url "https://gitlab.vba.ovh" \
    --registration-token "" \
    --executor "docker" \
    --description "Generic docker runner" \
    --docker-image "docker:latest" \
    --docker-privileged \
    --docker-volumes "/certs/client"\
    # Unsure below this line
    --tag-list "instance,vba.ovh,generic" \
    --locked "False" \
    --run-untagged "False" \
    --limit 0 \
    --access-level="not_protected" \
    --docker-network-mode web
