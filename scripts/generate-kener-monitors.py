#!/usr/bin/python3

import re
import yaml
import shutil


files=[
    "/appdata/docker-compose-lvl0.yml",
    "/appdata/docker-compose-lvl1.yml",
    "/appdata/docker-compose-lvl2.yml",
    "/appdata/docker-compose-lvl3.yml",
    "/appdata/docker-compose-lvl4.yml",
]

services = []

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text
def remove_suffix(input_string, suffix):
    if input_string.endswith(suffix):
        return input_string[:-len(suffix)]
    return input_string
def to_lower_kebabcase(s):
    return s.lower().replace(" ", "-").replace(".", "-")
def clean_host(s):
    return remove_suffix(s, ".${PUBLIC_DNS}")
    
regex = r"Host\(`(.+?)`\)"
for file in files:
    with open(file, "r") as yml:
        content = yaml.safe_load(yml)
        for key in content["services"]:
            image = ""
            urls = []
            for label in content["services"][key]["labels"]:
                if "homepage.description" in label:
                    description = remove_prefix(label, "homepage.description=")
                elif "homepage.group" in label:
                    tag = to_lower_kebabcase(remove_prefix(label, "homepage.group="))
                elif "homepage.name" in label:
                    name = remove_prefix(label, "homepage.name=")
                elif "kener.logo" in label:
                    image = remove_prefix(label, "kener.logo=")
                elif "rule=Host(" in label:
                    urls += list(map(clean_host, re.findall( regex, label)))
            services.append((name,description,tag,image,urls))   
            
for srv in services:
    if(len(srv[4])) == 0:
        continue
    
    url=min(srv[4], key=len)           
    print("- name: "+srv[0]+"")
    print("  description: "+srv[1]+"")
    print("  image: /"+srv[0]+".png")
    print("  tag: \""+to_lower_kebabcase(srv[2]+"-"+srv[0])+"\"")
    print("  api:")
    print("      method: GET")
    print("      url: https://"+url+".vba.ovh")
        