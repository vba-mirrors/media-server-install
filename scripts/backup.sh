#!/bin/bash


set -x


# Install NTFY
cd /tmp
wget https://github.com/binwiederhier/ntfy/releases/download/v1.27.2/ntfy_1.27.2_linux_x86_64.tar.gz
tar zxvf ntfy_1.27.2_linux_x86_64.tar.gz
sudo cp -a ntfy_1.27.2_linux_x86_64/ntfy /usr/bin/ntfy
sudo mkdir -p /etc/ntfy && sudo cp ntfy_1.27.2_linux_x86_64/{client,server}/*.yml /etc/ntfy


###################
####  HELPERS  ####
###################
on_error(){
    log_error "Uncautgh error - $BASH_COMMAND" /tmp/backup_logs/final_temp.log
}
log_separator(){
        echo "=============="
        echo "=============="
        echo "=============="
        ntfy publish -t "===============" -p 2 -tags=bug ntfy.vba.ovh/vba-vizbab-log "====================================
====================================
====================================" >> /dev/null
}
log_debug(){
    echo -e "\e[32m${LOG_PREFIX}$1"
    if [ "$2" == "" ]; then
        ntfy publish -t "${LOG_PREFIX}DEBUG" -p 2 -tags=bug ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
    else
        ntfy publish -t "${LOG_PREFIX}DEBUG" -p 2 -tags=bug -file=$2 ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
        echo "=============="
        cat $2
    fi
    echo -e "\e[39m"
}
log_success(){
    echo -e "\e[96m${LOG_PREFIX}$1"
    if [ "$2" == "" ]; then
        ntfy publish -t "${LOG_PREFIX}SUCCESS" -p 5 -tags=green_circle ntfy.vba.ovh/vba-vizbab "$1" >> /dev/null
        ntfy publish -t "${LOG_PREFIX}SUCCESS" -p 5 -tags=green_circle ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
    else
        ntfy publish -t "${LOG_PREFIX}SUCCESS" -p 5 -tags=green_circle -file=$2 ntfy.vba.ovh/vba-vizbab "$1" >> /dev/null
        ntfy publish -t "${LOG_PREFIX}SUCCESS" -p 5 -tags=green_circle -file=$2 ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
        echo "=============="
        cat $2
    fi
    echo -e "\e[39m"
}
log_info(){
    echo -e "\e[96m${LOG_PREFIX}$1"
    if [ "$2" == "" ]; then
        ntfy publish -t "${LOG_PREFIX}INFO" -p 3 -tags=blue_circle ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
    else
        ntfy publish -t "${LOG_PREFIX}INFO" -p 3 -tags=blue_circle -file=$2 ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
        echo "=============="
        cat $2
    fi
    echo -e "\e[39m"
}
log_warn(){
    echo -e "\e[93m${LOG_PREFIX}$1"
    if [ "$2" == "" ]; then
        ntfy publish -t "${LOG_PREFIX}WARN" -p 4 -tags=warning ntfy.vba.ovh/vba-vizbab "$1" >> /dev/null
        ntfy publish -t "${LOG_PREFIX}WARN" -p 4 -tags=warning ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
    else
        ntfy publish -t "${LOG_PREFIX}WARN" -p 4 -tags=warning -file=$2 ntfy.vba.ovh/vba-vizbab "$1" >> /dev/null
        ntfy publish -t "${LOG_PREFIX}WARN" -p 4 -tags=warning -file=$2 ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
        echo "=============="
        cat $2
    fi
    echo -e "\e[39m"
}
log_error(){
    echo -e "\e[31m${LOG_PREFIX}$1"
    if [ "$2" == "" ]; then
        ntfy publish -t "${LOG_PREFIX}ERROR" -p 5 -tags=sos ntfy.vba.ovh/vba-vizbab "$1" >> /dev/null
        ntfy publish -t "${LOG_PREFIX}ERROR" -p 5 -tags=sos ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
    else
        ntfy publish -t "${LOG_PREFIX}ERROR" -p 5 -tags=sos -file=$2 ntfy.vba.ovh/vba-vizbab "$1" >> /dev/null
        ntfy publish -t "${LOG_PREFIX}ERROR" -p 5 -tags=sos -file=$2 ntfy.vba.ovh/vba-vizbab-log "$1" >> /dev/null
        echo "=============="
        cat $2
    fi
    echo -e "\e[39m"
}

run_backup(){
    source="$1"
    target="$2"
    echo "Copying $source into remote folder $target"


    # Generate exclude params
    excludes=()
    if [ $# -ge 3 ]; then
        excludes+=(--exclude "$3")
        while shift && [ -n "$3" ]; do
            excludes+=(--exclude "$3")
        done
    else
        excludes+=(--exclude /tmp)
    fi

    echo "" > /tmp/backup_logs/rsync_temp.log
    date &>>/tmp/backup_logs/rsync_temp.log    
    echo "Excluding flags: ${excludes[@]}" >> /tmp/backup_logs/rsync_temp.log
    echo "" >> /tmp/backup_logs/rsync_temp.log

    sudo rsync -rltDzvh \
        --delete-after \
        --stats \
        "${excludes[@]}" \
        "$source" \
        "$target" &>>/tmp/backup_logs/rsync_temp.log
    retVal=$?

    date &>>/tmp/backup_logs/rsync_temp.log
    if [ $retVal -ne 0 ]; then
        echo "Error while copying $source into remote folder $target"
    else
        echo "Copied $source into remote folder $target"
    fi        
}


###################
#####  SETUP  #####
###################
mkdir -p /tmp/backup_logs/
log_separator
log_separator


###################
###  PRE-CHECK  ###
###################
log_separator
log_info "Backup script initialisation started at $(date)"
LOG_PREFIX="PRE-CHECK - "

mkdir -p /tmp/dummy
echo "dummy" > /tmp/dummy/save-ignored
echo "dummy" > /tmp/dummy/save\ non\'standard\ ignored
echo "dummy" > /tmp/dummy/save\ non\'standard\ kept
echo "dummy" > /tmp/dummy/save-test

# Testing all kind of logs
log_debug "Logs are working" /tmp/dummy/save-test

# Testing we log failures
ls /tmp/error\ catcher\ should\ work

# Testing remote copy
run_backup "/tmp/dummy/" "/dummy" "save-ignored" "save non'standard ignored"

LOG_PREFIX=""
log_debug "End of test phase"



###################
#####  START  #####
###################
log_separator
log_info "Backup script execution started at $(date)"

log_info "Rotating old backups"
mkdir -p /shares/backup/mediaserver/delta_1
mkdir -p /shares/backup/mediaserver/delta_0
rm -r /shares/backup/mediaserver/delta_2/
mv /shares/backup/mediaserver/delta_1 /shares/backup/mediaserver/delta_2
mv /shares/backup/mediaserver/delta_0 /shares/backup/mediaserver/delta_1
mkdir /shares/backup/mediaserver/delta_0


log_info "Stopping services before backup"
cd /appdata
docker compose down

echo "" > /tmp/backup_logs/final_temp.log
echo "Started  @ $(date)" >> /tmp/backup_logs/final_temp.log


##########  Source                        Destination                           Exclusion                                              
run_backup "/appdata"                   "/shares/backup/mediaserver/delta_0"    "data/plex/database" "data/mariadb/log" "log"                                                                                                               


echo "Starting services"
cd /appdata
export DOCKER_CLIENT_TIMEOUT=120
export COMPOSE_HTTP_TIMEOUT=120
export COMPOSE_PARALLEL_LIMIT=30
docker compose up -d
sleep 300 # Wait 5 minute

###################
######  END  ######
###################
log_separator
echo "Finished @ $(date)" >> /tmp/backup_logs/final_temp.log
log_info "Backup script execution finished at $(date)" /tmp/backup_logs/final_temp.log

echo $(date)                     > /shares/backup/mediaserver/delta_0/backup_finished_at
cp /tmp/backup_logs/final_temp.log /shares/backup/mediaserver/delta_0/backup_log