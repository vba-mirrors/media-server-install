#!/bin/bash

# Prerequistes
apt-get --assume-yes update
apt-get --assume-yes install sudo curl ncdu nfs-common rsync htop tree dos2unix gnupg2 tzdata python3-pip smartmontools
pip install uptime_kuma_api load_dotenv


# Register shares to mount
mkdir -p /shares/isos
mkdir -p /shares/media
mkdir -p /shares/backup
mkdir -p /shares/private
mkdir -p /shares/public
echo "" >> /etc/fstab
echo "" >> /etc/fstab
echo "192.168.0.100:/mnt/user/isos /shares/isos nfs rw,vers=3,proto=tcp,sec=sys,mountaddr=192.168.0.100,addr=192.168.0.100 0 0" >> /etc/fstab
echo "192.168.0.100:/mnt/user/media /shares/media nfs rw,vers=3,proto=tcp,sec=sys,mountaddr=192.168.0.100,addr=192.168.0.100 0 0" >> /etc/fstab
echo "192.168.0.100:/mnt/user/backup /shares/backup nfs rw,vers=3,proto=tcp,sec=sys,mountaddr=192.168.0.100,addr=192.168.0.100 0 0" >> /etc/fstab
echo "192.168.0.100:/mnt/user/private /shares/private nfs rw,vers=3,proto=tcp,sec=sys,mountaddr=192.168.0.100,addr=192.168.0.100 0 0" >> /etc/fstab
echo "192.168.0.100:/mnt/user/public /shares/public nfs rw,vers=3,proto=tcp,sec=sys,mountaddr=192.168.0.100,addr=192.168.0.100 0 0" >> /etc/fstab

# Mount
mount -a

# Configure auto mount after network initialized
echo "" >> /etc/network/interfaces
echo "" >> /etc/network/interfaces
echo "post-up mount -a" >> /etc/network/interfaces

# Setup backups cron
echo "20 4 1,15 * *   root     /appdata/scripts/backup.sh" >> /etc/crontab
echo "*  * *    * *   root     /appdata/scripts/test.sh" >> /etc/crontab








# Install Docker
# https://docs.docker.com/engine/install/debian/
cd /tmp
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
apt-get --assume-yes update
apt-get --assume-yes install apt-transport-https ca-certificates curl gnupg-agent software-properties-common curl
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get --assume-yes update
apt-get --assume-yes install docker-ce docker-ce-cli containerd.io
docker -v

## Install Compose
## https://docs.docker.com/compose/install/
#curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
#chmod +x /usr/local/bin/docker-compose
#ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
#docker-compose --version

# Insatll CTOP
# https://github.com/bcicen/ctop
echo "deb http://packages.azlux.fr/debian/ buster main" | tee /etc/apt/sources.list.d/azlux.list
wget -qO - https://azlux.fr/repo.gpg.key | apt-key add -
apt update
apt install docker-ctop

# Insatll better docker ps
# https://github.com/Mikescher/better-docker-ps
sudo wget "https://github.com/Mikescher/better-docker-ps/releases/latest/download/dops_linux-amd64-static" -O "/usr/local/bin/dops" && sudo chmod +x "/usr/local/bin/dops"

# Add media user to docker group
/usr/sbin/usermod -aG docker media






# Prepare for first run
mkdir -p /appdata
rsync -avh --progress --delete /shares/isos/media\ server\ install/scripts/ /appdata/scripts/
cp /shares/isos/media\ server\ install/resources/.env /appdata/.env
chmod u+x /appdata/scripts/*.sh
chown -R media:media /appdata


# Switch user, setup its bash env and fire first run !
/usr/sbin/usermod -aG sudo media
su media
sudo dos2unix /appdata/scripts/**/*.sh
sudo dos2unix /appdata/scripts/*.sh
/appdata/scripts/refresh_local_env.sh
sudo cp -r /appdata/resources/cutom_bash_profile.sh ~/cutom_bash_profile
echo "" >>  ~/.bashrc
echo "" >>  ~/.bashrc
echo "source ~/cutom_bash_profile" >>  ~/.bashrc
sudo dos2unix ~/cutom_bash_profile
source ~/.bashrc

/appdata/scripts/refresh_local_env.sh

.s
