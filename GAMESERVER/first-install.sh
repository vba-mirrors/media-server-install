#!/bin/bash

# Prerequistes
apt-get --assume-yes update
apt-get --assume-yes install sudo curl ncdu nfs-common rsync htop tree dos2unix gnupg2 tzdata
 

# Install Docker
# https://docs.docker.com/engine/install/debian/
cd /tmp
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
apt-get --assume-yes update
apt-get --assume-yes install apt-transport-https ca-certificates curl gnupg-agent software-properties-common curl
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get --assume-yes update
apt-get --assume-yes install docker-ce docker-ce-cli containerd.io
docker -v
systemctl enable --now docker


# Insatll CTOP
# https://github.com/bcicen/ctop
sudo apt-get install ca-certificates curl gnupg lsb-release
curl -fsSL https://azlux.fr/repo.gpg.key | sudo gpg --dearmor -o /usr/share/keyrings/azlux-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/azlux-archive-keyring.gpg] http://packages.azlux.fr/debian \
  $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/azlux.list >/dev/null
sudo apt-get update
sudo apt-get install docker-ctop

# Insatll better docker ps
# https://github.com/Mikescher/better-docker-ps
sudo wget "https://github.com/Mikescher/better-docker-ps/releases/latest/download/dops_linux-amd64-static" -O "/usr/local/bin/dops" && sudo chmod +x "/usr/local/bin/dops"


# Add media user to docker group
/usr/sbin/usermod -aG docker media

