export DOCKER_CLIENT_TIMEOUT=120
export COMPOSE_HTTP_TIMEOUT=120
export COMPOSE_PARALLEL_LIMIT=30


cd /appdata

alias c='clear'
alias l='ls -ahl'
alias d='docker'
alias dc='docker compose'
alias subdirs_size='sudo du -hd 1 .'

alias .s='.s3'
alias .s0='docker compose --profile lvl0 up --detach --remove-orphans'
alias .s1='docker compose --profile lvl1 --profile lvl0 up --detach --remove-orphans'
alias .s2='docker compose --profile lvl2 --profile lvl1 --profile lvl0 up --detach --remove-orphans'
alias .s3='docker compose --profile lvl3 --profile lvl2 --profile lvl1 --profile lvl0 up --detach --remove-orphans'
alias .s4='docker compose --profile lvl4 --profile lvl3 --profile lvl2 --profile lvl1 --profile lvl0 up --detach --remove-orphans'
alias .sall='docker compose --profile all up --detach --remove-orphans'
alias .u='docker compose --profile all pull'
alias .rf='/appdata/scripts/refresh_local_env.sh'
alias .stop='docker compose --profile all down'
alias .u='docker compose pull'

alias .rs='.rf && .s'
alias .restart='.stop && .s'
alias .full-restart='.rf && .stop && .u && .s && .logs'

alias .logs='docker compose logs -f --tail 100'
alias .clean='docker system prune -a'
alias .refresh-auth='.rf && docker restart authelia'
alias .restart-vpn-dependents='d stop qbittorrent && d rm qbittorrent && dc up -d qbittorrent'

alias .util_folder_size='sudo du -hs'

alias .detect_port='sudo lsof -i:$1'
alias .check_vpn='wget -qO- https://ipinfo.io'
alias .check_gitlab_perms='docker exec -it gitlab update-permissions'
alias .check_gitlab_perms='docker exec -it gitlab gitlab-ctl registry-garbage-collect -m'
.dsh() {
    ss
    docker exec -it $1 sh
}

.dbash() {
    docker exec -it $1 bash
}

